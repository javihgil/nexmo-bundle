<?php
namespace Jhg\NexmoBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Bundle.
 * @author Javi Hernández Gil <bilbo@deverbena.com>
 */
class JhgNexmoBundle extends Bundle
{
	
}
